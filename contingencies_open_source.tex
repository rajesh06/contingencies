\documentclass[]{article}
\usepackage{url}
\usepackage{svg}

%opening
\title{The Open Source Software Revolution in Actuarial Science}
\author{Rajesh Sahasrabuddhe and Steve Berman}

\begin{document}

\maketitle

%\begin{abstract}
%
%\end{abstract}

\section{Introduction}
Most actuarial departments are ``Microsoft shops'' or ``SAS shops'' or ``AXIS shops'' That is, they rely on those proprietary software applications to meet their modeling and reporting needs. The vendor that developed the software controls all aspects of the application including the source code, distribution, and upgrade cycle. The end-user then purchases licenses to use the proprietary application from the developer.

But it has not always been this way. Technology firms were initially hardware-focused. The earliest ``software'' was code developed in the academic community. That code was freely shared so that research could be both furthered and reproduced.  Ironically in those early days, open source software was assumed to not be malicious because the code was included.  In this article, we explore the return of this ``opens source'' software model.

\subsection{The Evolution from Proprietary Applications to Open Source}
The proprietary software model evolved when technology firms recognized the revenue potential of software. Through the introduction of the Lisa and Macintosh computers, Apple showed the world that software sells hardware rather that the other way around. Commercial software applications became a revenue stream. The graphical interface of Microsoft Windows 2.0 (and the improved interface of Windows 3.0) opened the floodgates of that revenue stream as software could now be used by and sold to the masses. That is, with the graphical interface, the use of software no longer required any programming knowledge. 
% Delete this: Eventually software like WordPerfect and Lotus 1-2-3 (for the more ``seasoned'' reader) gave way to Microsoft Excel and Word.

Actuarial firms were not blind to this sea change. Eventually, actuarial consulting firms developed proprietary applications. For some firms, actuarial software (and the associated support) was not a side business - it was their \emph{only} business. 

While this phenomenon of course did not only occur in the actuarial community, the development of domain-specific software was an indication that ``platforms for the masses'' either did not include all of the functionality required and/or that there was value in creating applications built on those generic platforms that facilitated specific types of analysis. For many, the development proprietary domain-specific software applications meant that their problems were solved. 

But there are always skeptics. Those skeptics want to be able to verify that the software is performing the calculation that they are expecting. They also want to be able to modify the functionality to meet their specific needs. They do not want to rely on a commercial vendor to improve capabilities or implement enhancements. Open source software addressed these concerns. 

\section{What is Open Source Software}
\subsection{Is Open Source Software Free?}
``Open source'' refers to a program in which the source code is available to the general public for use and/or modification from its original design. Open source software is generally - but not always - free of charge.  There are semantic differences between the term ``Open Source Software'' and ``Free Software.'' The former refers to the condition that source code for the software is made available to the licensee. ``Free'' in the latter term refers to \emph{freedom} not cost. A more through discussion of the semantic differences of various types of free and open source software is available at \url{https://www.gnu.org/philosophy/categories.html}. 

The Venn diagram in Figure \ref{fig:category} from \url{http://www.gnu.org/philosophy/categories.en.html} may be useful in understanding the differences between various categories of software.
%\includesvg{/img/category.svg}

\begin{figure}[h]
\centering
\includegraphics[width=0.7\linewidth]{img/category}
%\includesvg{img/category.svg}
\caption{Software Application Categories}
\label{fig:category}
\end{figure}

We use ``Open Source Software'' to refer to applications that are distributed with the source code with or without cost. This principle is embodied in the licenses that accompany open-source software. Two of the more common licenses are the GNU General Public License (commonly referred to as the GPL License) and the MIT License which include the following.

\begin{quotation}
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the ``Software''), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or \emph{sell} copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.\\
	https://opensource.org/licenses/MIT (emphasis added)
\end{quotation} 

\begin{quote}
	The GNU General Public License is a free, copyleft\footnote{Copyleft is a general method for making a program (or other work) free, and requiring all modified and extended versions of the program to be free as well. ...  So instead of putting GNU software in the public domain, we “copyleft” it. Copyleft says that anyone who redistributes the software, with or without changes, must pass along the freedom to further copy and change it. Copyleft guarantees that every user has freedom. per \url{http://www.gnu.org/licenses/copyleft.en.html}} license for software and other kinds of works.
	
	The licenses for most software and other practical works are designed to take away your freedom to share and change the works.  By contrast,	the GNU General Public License is intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users.  We, the Free Software Foundation, use the GNU General Public License for most of our software; it applies also to any other work released this way by its authors.  You can apply it to your programs, too.
	
	When we speak of free software, we are referring to freedom, not price.  Our General Public Licenses are designed to make sure that you have the freedom to distribute copies of free software \emph{(and charge for them if you wish)}, that you receive source code or can get it if you want it, that you can change the software or use pieces of it in new free programs, and that you know you can do these things.\\
	https://www.gnu.org/licenses/gpl.html (emphasis added)
\end{quote}

\subsection{The Benefits of Open Source}
The primary benefit of open source software is that it allows a disjoint community of programmers to work collaboratively to improve the code and share the fruits of their labor.  This was one of the rationales for the open source movement. That is, that a larger group of programmers not concerned with proprietary ownership or, in many cases, financial gain will produce a more useful and bug-free product for everyone to use.  Due to the larger programmer base of the community and the ``as-is'' licensing, needed fixes might be identified earlier and improvements implemented more quickly.  Common applications that are open-source include the Android and Linux operating systems and the Mozilla Firefox browser. As a result, open source is a good alternative is situations where internal resources to support a specialized group are limited.


\subsection{Open Source Tools used by Actuaries}
The most common open source tool used by property/casualty actuaries is R.  CAS training courses using R commenced as early as 2006 and continue today. Articles started appearing in the actuarial literature shortly thereafter.  Today, in addition to dedicated training seminars, other CAS meetings and seminars offer workshops to train actuaries in R.

The use of R includes some well-known packages written by actuaries, including the chainladder package (for loss reserving) and actuar (for loss distributions and other functions).  Actuaries also leverage more commonly used packages in R, including data visualization and geospatial capabilities.  However, R is not the only tool.  Python is growing in popularity, particularly for web searching, and other tools like Julia and F\# can be leveraged.

For life and health analyses, there are several applications that are commonly used that contain open-source components.  These include Towers Watson's MoSes and Prophet from Sungard.  Both of these must be licensed from the vendor.  Code is provided in the form of libraries or packages, which can be changed to suit the organization's needs.

\section{Commercial Support}
While open source offers flexibility and accessibility to a large code base, it is not necessarily consistent with all of the needs of an enterprise development environment. This gap is filled by commercial software providers.  These providers supplement open-source with in-house developed functions, sometimes enhanced for big data and/or parallel processing. Software maintenance, technical support and access to experts are available as part of the license or as an add-on service.  Support is also available in the community at large, though message boards and websites, some of which are maintained by the commercial company. %  For R, Revolution Analytics is the primary provider. (Too narrow a reference)

\section{Compliance Issues}
The use of open source software raises compliance issues - both as respects internal company standard and, more broadly, Actuarial Standards of Practice. In both areas the primary issue relates to the reality that most actuaries are not programmers. 

\subsection{Internal Standards: Peer Review}
Whether open source is used primarily in the actuarial group or throughout the entire company, steps must be taken to ensure that the internal needs are met.  This includes correctness of the code, security, and protection of intellectual property.  Issues related to security primarily fall to administrators, sometimes with assistance from the commercial vendors. Issues of intellectual property are generally addressed by legal departments and senior management. This leaves the code to the actuarial department. Those departments will generally have peer review requirements. As a result, this requires that departments identify ``coder-actuaries'' and develop standards for the review of code. The coder-actuary group will generally evolve into an internal community. Effective independent peer review will require that some members of the community remain independent of each project so that they can provide a ``fresh set of eyes.'' It will also require that the department establish processes and procedures for code review similar to those that generally exist in IT departments but are may be unknown to actuarial departments.

\subsection{Professional Standards}
Actuarial Standard of Practice No. 41, Actuarial Communications states that "In the actuarial report, the actuary should state the actuarial findings, and identify the methods, procedures, assumptions, and data used by the actuary with sufficient clarity that another actuary qualified in the same practice area could make an objective appraisal of the reasonableness of the actuary's work as presented in the actuarial report."

Much of the success of productivity software suites (such as Microsoft Office) is due to their ability to facilitate exhibits and documentation production that are ``universally reviewable.'' That is, generally they require no specific programming knowledge to use. That, in a sense, is why those suites have been the preferred tool for the production of actuarial reports that comply with ASOP 41.

Many open source tools were designed around principles of reproducible research. If you are not familiar with this concept, it is discussed on R-Website (known as CRAN, The Comprehensive R Archive Network) as follows:

\begin{quote}
The goal of reproducible research is to tie specific instructions to data analysis and experimental data so that scholarship can be recreated, better understood and verified.

R largely facilitates reproducible research using literate programming; a document that is a combination of content and data analysis code. The Sweave function (in the base R utils package) and the knitr package can be used to blend the subject matter and R code so that a single document defines the content and the algorithms.
\end{quote}

That is, they are intended to allow another user to reproduce - and more importantly test - the results of the original researcher. That should naturally allow for a report produced comply with ASOP 41 - but do they? Those reports will provide the commands compiled by the software to produce the analysis and the associated results / output. As such, the algorithm presented in the report will be ``code-based'' rather than ``exhibit-based.'' However, is it fair to assume or expect that \emph{another actuary qualified in the same practice area} should be able to interpret the code-based algorithm. This may be an issue that the professional will need to deal with in the future.

\section{Conclusions}

Open source, at one time an oddity that was primarily found in academia, is now becoming a feasible tool in the business world and specifically in the actuarial community.  Proper usage can accelerate the development process, possibly at lower costs.  Due diligence must be exercised, however, to ensure that critical issues, like creating a secure environment and maintaining professional standards, are met.  Actuaries should evaluate on a case-by-case basis whether open-source is the best choice for their group.

\end{document}
